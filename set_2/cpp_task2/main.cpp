//15 min

#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n, a, b;
    int total = 0;
    int shots = 0;
    cin >>n >>a >>b;
    vector<int> forest;
    forest.resize(n);
    for (int i = 0; i < n; i++){
        cin >>forest[i];
        total += forest[i];
    }
    int row = 0;
    while (total > 0){
        if (row >= n){
            row = 0;
            continue;
        }
        while (forest[row] == 0){
            row++;
            if (row >= n){
                row = 0;
                continue;
            }
        }
        int pos = row;
        shots++;
        for(; row < pos + a && row < n; row++){
            total -= min(b, forest[row]);
            forest[row] -= min(b, forest[row]);
        }
    }
    cout <<shots;
}
