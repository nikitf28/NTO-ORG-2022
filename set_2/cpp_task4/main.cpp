//45 mins

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void simplify(int p1, int p2){
    int del = 2;
    while (del <= p1 && del <= p2){
        if (p1 % del == 0 && p2 % del == 0){
            p1 /= del;
            p2 /= del;
        }
        else{
            del++;
        }
    }
    cout <<p1 <<" " <<p2;
}

int main() {
    vector<int> ys;
    ys.resize(4);
    cin >>ys[0] >>ys[1] >>ys[2] >>ys[3];
    sort(ys.begin(), ys.end());
    int y;
    cin >>y;
    if (y <= ys[0]){
        cout <<"FUNC 1" <<endl;
        cout << 1 <<" " <<0;
        return 0;
    }
    if (y >= ys[3]){
        cout << 0 <<" " <<1;
        cout <<"FUNC 2" <<endl;
        return 0;
    }
    if (y >= ys[0] && y <= ys[1]){
        cout <<"FUNC 3" <<endl;
        int part1 = (y - ys[0]) * (ys[0] - ys[1]) * 2 / 2;
        int part2 = (ys[1] - y) * ();
        part2 += (ys[2] - ys[1])*2;
        part2 += (ys[3] - ys[2]);
        simplify(part2, part1);
        return 0;
    }
    if (y >= ys[2] && y <= ys[3]){
        cout <<"FUNC 4" <<endl;
        int part1 = ys[4] - y;
        int part2 = y - ys[3];
        part2 += (ys[2] - ys[1])*2;
        part2 += (ys[1] - ys[0]);
        simplify(part1, part2);
        return 0;
    }
    if (y >= ys[1] && y <= ys[2]){
        cout <<"FUNC 5" <<endl;
        int part1 = (y - ys[1])*2;
        part1 += ys[1] - ys[0];
        int part2 = (ys[2] - y)*2;
        part2 += ys[3] - ys[2];
        simplify(part2, part1);
        return 0;
    }
}
