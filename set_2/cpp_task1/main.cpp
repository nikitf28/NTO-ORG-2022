//15 min

#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    vector<double> parts, amount;
    cin >>n;
    parts.resize(n);
    amount.resize(n);
    for (int i = 0; i < n; i++){
        cin >>parts[i];
    }
    for (int i = 0; i < n; i++){
        cin >>amount[i];
    }
    double total = 9999999999999;
    for (int i = 0; i < n; i++){
        if (parts[i] == 0){
            continue;
        }
        total = min(total, amount[i] * 100 / parts[i]);
    }
    cout <<total;
}
