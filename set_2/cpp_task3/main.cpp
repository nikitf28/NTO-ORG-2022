//20 min

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    int n;
    cin >>n;
    vector<pair<int, int> > points;
    points.resize(3 * n, make_pair(0, 0));
    for (int i = 0; i < 3 * n; i++){
        string sample;
        cin >>sample;
        int lngth = 1;
        points[i].second = i;
        for (int j = 1; j < sample.length(); j++){
            if (sample[j - 1] != sample[j]){
                points[i].first += lngth*lngth;
                lngth = 0;
            }
            lngth++;
        }
        points[i].first += lngth*lngth;
    }
    sort(points.begin(), points.end());
    vector<char> answers;
    answers.resize(3 * n, '-');
    for (int i = 0; i < n; i++){
        answers[points[i].second] = 'B';
    }
    for (int i = n; i < 2*n; i++){
        answers[points[i].second] = 'A';
    }
    for (int i = 2*n; i < 3*n; i++){
        answers[points[i].second] = 'C';
    }
    for (int i = 0; i < 3 * n; i++){
        cout <<answers[i];
    }
}
