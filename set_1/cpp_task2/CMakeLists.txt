cmake_minimum_required(VERSION 3.21)
project(cpp_task2)

set(CMAKE_CXX_STANDARD 14)

add_executable(cpp_task2 main.cpp)
