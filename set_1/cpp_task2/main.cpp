#include <iostream>
#include <vector>

using namespace std;

void dfs(int i, int j, int col, vector<vector<int> > &data){
    if (i < 0 || j < 0 || i >= 50 || j >= 50){
        return;
    }
    if (data[i][j] == 0){
        data[i][j] = col;
        dfs(i + 1, j, col, data);
        dfs(i - 1, j, col, data);
        dfs(i, j + 1, col, data);
        dfs(i, j - 1, col, data);
        dfs(i + 1, j + 1, col, data);
        dfs(i + 1, j - 1, col, data);
        dfs(i - 1, j + 1, col, data);
        dfs(i - 1, j - 1, col, data);
    }
}

int main() {
    vector<vector<int> > data;
    int color = 2;
    data.resize(50);
    for (int i = 0; i < 50; i++){
        data[i].resize(50);
        for (int j = 0; j < 50; j++){
            cin >>data[i][j];
        }
    }

    for (int i = 0; i < 50; i++){
        for (int j = 0; j < 50; j++){
            if (data[i][j] == 0){
                dfs(i, j, color, data);
                color += 1;
            }
        }
    }

    cout <<color - 3;
}
