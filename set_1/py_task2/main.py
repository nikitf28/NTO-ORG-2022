# 50 min

import sys

sys.setrecursionlimit(3000)

data = []

color = 2


def dfs(i, j, col, data):
    if i < 0 or j < 0 or i >= 50 or j >= 50:
        return

    if data[i][j] == 0:
        data[i][j] = col
        dfs(i + 1, j, col, data)
        dfs(i - 1, j, col, data)
        dfs(i, j + 1, col, data)
        dfs(i, j - 1, col, data)
        dfs(i + 1, j + 1, col, data)
        dfs(i + 1, j - 1, col, data)
        dfs(i - 1, j + 1, col, data)
        dfs(i - 1, j - 1, col, data)


for i in range(50):
    data.append(list(map(int, input().split())))

for i in range(50):
    for j in range(50):
        if data[i][j] == 0:
            dfs(i, j, color, data)
            color += 1

print(color - 3)
