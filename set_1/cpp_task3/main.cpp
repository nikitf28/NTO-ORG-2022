#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

int summ = 0, w = 0, h = 0;

void dfs(int i, int j, int col, vector<vector<int> > &data, bool initial=false){
    if (i < 0 || j < 0 || i >= h || j >= w){
        return;
    }
    if (initial){
        summ = 0;
    }

    if (data[i][j] == 1){
        summ += 1;
        data[i][j] = col;
        dfs(i + 1, j, col, data);
        dfs(i - 1, j, col, data);
        dfs(i, j + 1, col, data);
        dfs(i, j - 1, col, data);
        dfs(i + 1, j + 1, col, data);
        dfs(i + 1, j - 1, col, data);
        dfs(i - 1, j + 1, col, data);
        dfs(i - 1, j - 1, col, data);
    }
}

void splitString(string str, vector<int> &splitted){
    string newStr = "";
    for (int i = 0; i < str.length(); i++){
        newStr += str[i];
        if (str[i] == ' '){
            splitted.push_back(stoi(newStr));
            newStr = "";
        }
    }
    if (newStr != ""){
        splitted.push_back(stoi(newStr));
    }
}

int main() {
    vector<vector<int> > data;
    int color = 2;
    map<int, int> coins;
    vector<int> coins_cost;

    cin >>h >>w;
    string input;
    getline(cin, input);
    getline(cin, input);
    splitString(input, coins_cost);
    sort(coins_cost.begin(), coins_cost.end());

    data.resize(h);
    for (int i = 0; i < h; i++){
        data[i].resize(w);
        for (int j = 0; j < w; j++){
            cin >>data[i][j];
        }
    }

    for (int i = 0; i < h; i++){
        for (int j = 0; j < w; j++){
            if (data[i][j] == 1){
                dfs(i, j, color, data, true);
                color += 1;
                if (coins.find(summ) == coins.end()){
                    coins[summ] = 0;
                }
                coins[summ] += 1;
            }
        }
    }

    int total_money = 0;

    int i = 0;

    for (const auto  &p : coins){
        //cout <<p.second <<" ";
        total_money += p.second * coins_cost[i];
        i++;
    }
    //cout <<endl;
    cout <<color - 2 <<" " <<total_money;
}
