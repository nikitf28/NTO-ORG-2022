#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<vector<int> > data;
    data.resize(20);
    for (int i = 0; i < 20; i++){
        data[i].resize(20);
        for (int j = 0; j < 20; j++){
            cin >>data[i][j];
        }
    }

    int sq = 0;

    for (int i = 0; i < 20; i++){
        for (int j = 0; j < 18; j++){
            if (data[i][j] == 1 && data[i][j+1] == 0 && data[i][j+2]==1){
                data[i][j+1] = 2;
            }
        }
    }

    for (int i = 0; i < 18; i++){
        for (int j = 0; j < 20; j++){
            if (data[i][j] == 1 && data[i+1][j] == 0 && data[i+2][j] == 1){
                data[i+1][j] = 2;
            }
        }
    }

    for (int i = 0; i < 20; i++){
        for (int j = 0; j < 20; j++){
            if (data[i][j] == 1 || data[i][j] == 2){
                sq += 1;
            }
        }
    }

    cout <<sq;
}
