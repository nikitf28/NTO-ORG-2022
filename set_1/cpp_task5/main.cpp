#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

struct coord{
    double x = 0, y = 0, z = 0;
    double distance(coord r){
        return sqrt((x - r.x)*(x - r.x) + (y - r.y)*(y - r.y) + (z - r.z)*(z - r.z));
    }
};

int main() {
    int n;
    double r;
    cin >>n >>r;
    vector<coord> coords;
    vector<vector<coord> > clouds;

    for (int i = 0; i < n; i++){
        coord c;
        cin >>c.x >>c.y >>c.z;
        coords.push_back(c);
    }

    while(coords.size() > 0){
        vector<pair<double, int>> distances;
        for (int i = 0; i < coords.size(); i++){
            distances.push_back(make_pair(coords[i].distance(coords[0]), i));
        }
        sort(distances.begin(), distances.end());
        vector<coord> cloud;
        vector<int> to_delete;
        for (int i = 0; i < 10; i++){
            cloud.push_back(coords[distances[i].second]);
            to_delete.push_back(distances[i].second);
        }
        sort(to_delete.begin(), to_delete.end());
        reverse(to_delete.begin(), to_delete.end());
        clouds.push_back(cloud);
        for (int i = 0; i < 10; i++){
            coords.erase(coords.begin() + to_delete[i]);
        }
    }

    int dots = 0;

    for (auto cloud : clouds){
        coord avg;
        for (int i = 0; i < 10; i++){
            avg.x += cloud[i].x;
            avg.y += cloud[i].y;
            avg.z += cloud[i].z;
        }
        avg.x /= 10;
        avg.y /= 10;
        avg.z /= 10;
        for (auto coord : cloud){
            if (coord.distance(avg) <= r){
                dots += 1;
            }
        }

    }
    cout <<dots;
}
