import sys

sys.setrecursionlimit(10000)

data = [[0 for i in range(31)] for j in range(31)]
centres = []
roads = 0

n = int(input())


def dfs(h, w, initial = False):
    if initial:
        data[h][w] = 10
    if data[h][w] == 0:
        return 0
    if data[h][w] == 2 and not initial:
        return 2
    if data[h][w] == 3:
        return 2
    if data[h][w] == 4:
        return 4
    data[h][w] = 4
    info1 = dfs(h, w - 1)
    info2 = dfs(h, w + 1)
    info3 = dfs(h - 1, w)
    info4 = dfs(h + 1, w)
    zeroes = 0
    if info1 == 0:
        zeroes += 1
    if info2 == 0:
        zeroes += 1
    if info3 == 0:
        zeroes += 1
    if info4 == 0:
        zeroes += 1

    if initial:
        data[h][w] = 2
        return
    if zeroes >= 3:
        data[h][w] = 0
    if info1 == 2 or info2 == 2 or info3 == 2 or info4 == 2:
        data[h][w] = 3



for i in range(n):
    h, w = map(int, input().split())
    data[h][w] = 1

for i in range(1, 31):
    for j in range(1, 31):
        if data[i][j - 1] == 1 and data[i + 1][j] == 1 and data[i - 1][j] == 1 and data[i][j + 1] == 1:
            data[i][j] = 2
            centres.append([i, j])

for centr in centres:
    dfs(centr[0], centr[1], True)

for i in range(1, 31):
    for j in range(1, 31):
        if data[i][j] != 0:
            roads += 1

print(len(centres), roads)