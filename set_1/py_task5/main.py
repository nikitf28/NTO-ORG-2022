import math


def distance(coord1, coord2):
    if len(coord1) != len(coord2):
        raise ArithmeticError
    dist_sum = 0
    for i in range(len(coord1)):
        dist_sum += (coord1[i] - coord2[i]) ** 2
    return math.sqrt(dist_sum)


n, r = input().split()
n = int(n)
r = float(r)

coords = []
clouds = []

for i in range(n):
    coord = tuple(map(float, input().split()))
    coords.append(coord)

while len(coords) > 0:
    distances = []
    for i in range(len(coords)):
        distances.append((distance(coords[0], coords[i]), i))
    distances.sort()
    cloud = []
    to_delete = []
    for i in range(0, 10):
        cloud.append(coords[distances[i][1]])
        to_delete.append(distances[i][1])
    # print(distances)
    to_delete.sort(reverse=True)
    clouds.append(cloud)
    for i in range(0, 10):
        coords.pop(to_delete[i])

dots = 0

for cloud in clouds:
    avgX, avgY, avgZ = 0, 0, 0
    for i in range(10):
        avgX += cloud[i][0]
        avgY += cloud[i][1]
        avgZ += cloud[i][2]
    avgX /= 10
    avgY /= 10
    avgZ /= 10
    sphere_centre = (avgX, avgY, avgZ)
    for dot in cloud:
        if distance(sphere_centre, dot) <= r:
            dots += 1

print(dots)
