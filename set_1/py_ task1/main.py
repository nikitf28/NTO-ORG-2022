#20 min

data = []

for i in range(20):
    data.append(list(map(int, input().split())))

sq = 0

for i in range(20):
    for j in range(18):
        if data[i][j] == 1 and data[i][j+1] == 0 and data[i][j+2] == 1:
            data[i][j+1] = 2

for i in range(18):
    for j in range(20):
        if data[i][j] == 1 and data[i+1][j] == 0 and data[i+2][j] == 1:
            data[i+1][j] = 2

for i in range(20):
    for j in range(20):
        if data[i][j] == 1 or data[i][j] == 2:
            sq += 1


print(sq)