# 40 min

import sys
#import matplotlib.pyplot as plt

sys.setrecursionlimit(41000)

data = []

color = 2

summ = 0

coins = {}


def dfs(i, j, col, data, initial=False):
    global summ, w, h
    if i < 0 or j < 0 or i >= h or j >= w:
        return

    if initial:
        summ = 0

    if data[i][j] == 1:
        summ += 1
        data[i][j] = col
        dfs(i + 1, j, col, data)
        dfs(i - 1, j, col, data)
        dfs(i, j + 1, col, data)
        dfs(i, j - 1, col, data)
        dfs(i + 1, j + 1, col, data)
        dfs(i + 1, j - 1, col, data)
        dfs(i - 1, j + 1, col, data)
        dfs(i - 1, j - 1, col, data)


h, w = map(int, input().split())
coins_cost = list(map(int, input().split()))
coins_cost.sort()

for i in range(h):
    data.append(list(map(int, input().split())))

for i in range(h):
    for j in range(w):
        if data[i][j] == 1:
            dfs(i, j, color, data, True)
            color += 1
            if summ not in coins:
                coins[summ] = 0
            coins[summ] += 1

sorted_coins = sorted(coins.items())

total_money = 0

#plt.imshow(data)
#plt.show()

for i in range(len(coins)):
    total_money += sorted_coins[i][1] * coins_cost[i]
print(color - 2, total_money)
