#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n;
    cin >>n;
    vector<int> columns;
    int height = 0;
    for (int i = 0; i < n; i++){
        int a;
        cin >>a;
        columns.push_back(a);
        height = max(a, height);
    }

    vector<vector<int> > map1, map2;
    map1.resize(height);
    map2.resize(height);

    for (int i = 0; i < height; i++){
        map1[i].resize(n, 0);
        map2[i].resize(n, 0);
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < columns[i]; j++){
            map1[j][i] = 1;
            map2[j][i] = 1;
        }
    }

    for (int i = 0; i < height; i++){
        bool water = false;
        for (int j = 0; j < n; j++){
            if (map1[i][j] == 1){
                water = true;
                continue;
            }
            if (map1[i][j] == 0 && water){
                map1[i][j] = 2;
            }
        }
    }

    for (int i = 0; i < height; i++){
        bool water = false;
        for (int j = n - 1; j >= 0; j--){
            if (map2[i][j] == 1){
                water = true;
                continue;
            }
            if (map2[i][j] == 0 && water){
                map2[i][j] = 2;
            }
        }
    }

    int s = 0;

    for (int i = 0; i < height; i++){
        for (int j = 0; j < n; j++){
            if (map1[i][j] == 2 && map2[i][j] == 2){
                s++;
            }
        }
    }
    cout <<s*10;
}
