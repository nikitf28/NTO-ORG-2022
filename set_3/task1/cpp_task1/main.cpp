#include <iostream>
#include <vector>

using namespace std;

int main() {
    int n, m;
    cin >>n >>m;
    vector<int> clicks;
    vector<vector<bool> > field;
    for (int i = 0; i < m; i++){
        int a;
        cin >>a;
        clicks.push_back(a);
    }

    field.resize(n);
    for (int i = 0; i < n; i++){
        field[i].resize(n, false);
    }

    for (auto click : clicks){
        int cl = click - 1;
        int row = cl / n;
        int column = cl % n;
        for (int i = 0; i < n; i++){
            field[row][i] = !field[row][i];
        }
        for (int i = 0; i < n; i++){
            if (i == row){
                continue;
            }
            field[i][column] = !field[i][column];
        }
    }

    int s = 0;

    for (int row = 0; row < n; row++){
        for (int column = 0; column < n; column++){
            if (field[row][column]){
                s+=1;
            }
        }
    }
    cout <<s;
}
