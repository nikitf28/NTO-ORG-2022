#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

void split(string str, vector<string> &splitted, char separator){
    string new_str = "";
    for (int i = 0; i < str.length(); i++){
        if (str[i] == separator && new_str.length() > 0) {
            splitted.push_back(new_str);
            new_str = "";
            continue;
        }
        new_str += str[i];
    }
    if (new_str.length() > 0){
        splitted.push_back(new_str);
    }
}

int main() {
    set<string> to_use;
    string first_line;
    getline(cin, first_line);
    vector<string> fl;
    split(first_line, fl, ' ');
    for (auto chem : fl){
        to_use.insert(chem);
    }

    vector<pair<vector<string>, vector<string> > > reactions;

    string input_line;
    while(getline(cin, input_line)){
        vector<string> data;
        split(input_line, data, '=');
        vector<string> data0, data1;
        split(data[0], data0, ' ');
        split(data[1], data1, ' ');
        reactions.push_back(make_pair(data0, data1));
    }
    int prev = 0;

    while(to_use.size() != prev){
        prev = to_use.size();
        for (int i = 0; i < reactions.size(); i++){
            bool f = true;
            for (auto chem : reactions[i].first){
                if (to_use.find(chem) == to_use.end()){
                    f = false;
                    break;
                }
            }
            if (f){
                for (auto chem : reactions[i].second){
                    to_use.insert(chem);
                }
            }
        }
    }

    for (auto chem : to_use){
        cout <<chem <<" ";
    }
}
