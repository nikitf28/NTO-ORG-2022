#include <iostream>
#include <vector>

using namespace std;

int n, m;
vector<vector<int> > labirint;

void dfs(int row, int col){
    if (row < 0 || row > m || col < 0 || col > n || labirint[row][col] != 0){
        return;
    }
    labirint[row][col] = 2;
    dfs(row - 1, col);
    dfs(row, col - 1);
    dfs(row, col + 1);

}

int main() {
    cin >>n >>m;
    labirint.resize(m);
    for (int i = 0; i < m; i++){
        labirint[i].resize(n);
        for (int j = 0; j < n; j++){
            cin >>labirint[i][j];
        }
    }

    for (int i = 0; i < n; i++){
        dfs(m - 1, i);
    }

    int s = 0;

    for (int i = 0; i < m; i++){
        for (int j = 0; j < n; j++){
            if (labirint[i][j] == 0){
                s++;
            }
        }
    }
    cout <<s;
}
